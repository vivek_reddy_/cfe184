#include <stdio.h>
int main()
{
    int n, i, range;
    printf("Enter a number to be mutiplied: ");
    scanf("%d", &n);
    printf("Enter the range of multiplication: ");
    scanf("%d", &range);
    for (i = 1; i <= range; ++i) 
    {
        printf("%d * %d = %d \n", n, i, n * i);
    }
    return 0;
}