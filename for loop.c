#include <stdio.h>  
   
int main()
{  
    int number; 
    printf("Even numbers between 1 to 100\n");  
    
    for(number = 1;number <= 100; number++) {  
        
        if(number%2 == 0)
        { 
            
            printf("%d ", number);  
        }  
    }  
   
    return 0;  
} 