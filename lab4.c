#include <stdio.h>
int main()
{
   int n, m, sum = 0, remainder;
   printf("Enter an integer\n");
   scanf("%d", &n);
   m = n;
   while (m != 0)
   {
   remainder = m % 10;
   sum       = sum + remainder;
   m         = m / 10;
   }
   printf("Sum of digits of %d = %d\n", n, sum);
   return 0;
}

