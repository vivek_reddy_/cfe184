#include <stdio.h>
struct employee
{
    char    name[30];
    int     empId;
    float   salary;
    int     DOJ;
};
int main()
{
    struct employee emp;
    printf("\nEnter details :\n");
    printf("Name :");  
    scanf("%s",&emp.name);
    printf("ID :");         
    scanf("%d",&emp.empId);
    printf("Salary :");   
    scanf("%f",&emp.salary);
    printf("DOJ : ");        
    scanf("%d",&emp.DOJ);
    printf("\nEntered detail is:");
    printf("Name: %s\n"   ,emp.name);
    printf("Id: %d\n"     ,emp.empId);
    printf("Salary: %f\n",emp.salary);
    printf("DOJ: %d\n",emp.DOJ);
    return 0;
}